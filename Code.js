// @ts-nocheck
const gap = 5; //size of new Array/length of custom headers [dropped by|price history|poistunut|watching|days till sold]. If you add anything new, increase this

//NEXT: ADD https://asunnot.oikotie.fi/vuokrattavat-asunnot referrer in getJSON function (maybe with mode as input)
function checkTheseSheets() {
  var ss = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ");
  var sheets = { //Itäranta + Tapiola (3-4h, oma tontti, parveke):
    "mI": '100&conditionType%5B%5D=1&conditionType%5B%5D=2&limit=24&locations=%5B%5B14821,5,%2202100,+Espoo%22%5D,%5B14822,5,%2202110,+Espoo%22%5D%5D&lotOwnershipType%5B%5D=1&price%5Bmax%5D=600000&price%5Bmin%5D=200000&roomCount%5B%5D=3&roomCount%5B%5D=4&size%5Bmin%5D=50&offset=0',  //Itäranta/Tapiola sales
    "vI": '101&limit=24&locations=%5B%5B14821,5,%2202100,+Espoo%22%5D,%5B14823,5,%2202120,+Espoo%22%5D,%5B14822,5,%2202110,+Espoo%22%5D%5D&price%5Bmax%5D=1600&price%5Bmin%5D=600&roomCount%5B%5D=3&roomCount%5B%5D=4&size%5Bmin%5D=50&offset=0',          //Itäranta/Tapiola rentals
    "vL": '101&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D%5D&offset=0', //All Laru rentals
    "vLf": '101&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D%5D&roomCount%5B%5D=3&roomCount%5B%5D=4&size%5Bmin%5D=50&offset=0',
    "mLf": "100&conditionType%5B%5D=1&conditionType%5B%5D=2&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D,%5B14714,5,%2200340,+Helsinki%22%5D%5D&lotOwnershipType%5B%5D=1&price%5Bmax%5D=600000&price%5Bmin%5D=150000&roomCount%5B%5D=3&size%5Bmin%5D=35&offset=0", //Lauttasaari filtered (3h, oma tontti, parveke)
    "mL": "100&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D%5D&offset=0"                         //All Laru flats (no filters)
  };
  var sh, cards = [], url = "", type = { 100: "myyttävät", 101: "vuokrattavat" }, mode = "";

  Object.keys(sheets).forEach((s) => {
    sh = ss.getSheetByName(s);
    url = sheets[s];
    mode = type[url.slice(0, 3)]
    cards = getAllCards(url);
    if (sh.getLastRow() === 0) { //If the sheet is empty
      copyHeader(ss,sh,mode);
      populateSheetFirstTime(sh, cards, mode); //For new sheets
    }
    else {
      checkForChanges(cards, sh, mode);
    }
  });
}

function checkForChanges(cards, sh, mode) {
  var data = sh.getRange("A1").getDataRegion().getValues();
  var out = [], extr = [], curIDs = [], col = {};

  if (data.length != 1) { //Run only if there's info on the sheet already
    var curPrices = [], visits = [], poistunut = "", watching = "", msg = "", oldID = 0, oldIDs = [], coordinates = [];
    for (c of cards) {
      curIDs.push(c.id);
      curPrices.push(c.price);
      visits.push(c.visits);
      coordinates.push([c.coordinates.latitude, c.coordinates.longitude]);
    }
    data.forEach((row, idx) => {
      if (!idx) {
        row.forEach((x, idx) => col[x] = idx); //{price=2.0, published=9.0,description=7.0, address=1.0, ...} header columns object
      }
      poistunut = row[col.poistunut];
      oldID = row[col.id];
      oldIDs.push(oldID);
      //Turn this below into a function
      if (poistunut == "") { //Check for oldID matches in curIDs 
        var matchedAt = curIDs.indexOf(oldID);
        var pubDate = row[col.published];

        if (matchedAt !== -1) { //a. match found -> check price
          var curPrice = toNumber(curPrices[matchedAt]);

          if (mode === "myyttävät") {
            var oldPrice = row[col['Velaton hinta']];
            if (oldPrice != curPrice) {
              var diff = daysBetweenTwoDates(pubDate);
              row[col['price history']] += oldPrice + " for " + diff + " days/"; //priceHistory
              row[col['Velaton hinta']] = curPrice; //price

              //Difference in price
              var priceDiff = (oldPrice - curPrice);
              var k = priceDiff / 1000;
              var dropPercent = Math.round(10 * 100 * priceDiff / oldPrice) / 10; //The extra (10*...)/10 is to get one decimal e.g. 2.5%

              //Dropped by
              var newValues = extractHtmlElements(oldID, mode, undefined, ["Neliöhinta", "Myyntihinta"]);
              var oldNeliohinta = row[col['Neliöhinta']];
              var newNeliohinta = newValues[0];
              var dropPerMeter = Math.round(oldNeliohinta - newNeliohinta);
              var droppedBy = "↓" + dropPercent + "% (" + k + "k or " + dropPerMeter + "€/m )│ "; //↓10% (20.5k or 500€/m) │ 
              row[col['dropped by']] += droppedBy;
              row[col['Neliöhinta']] = newNeliohinta;
              row[col['Myyntihinta']] = newValues[1];

              //Send alert if watching
              watching = row[col.watching];
              if (watching == "x") {
                priceDiff = oldPrice - curPrice;
                msg += row[col.address] + " has lowered its price by " + priceDiff + "€! <a href=" + row[col.url] + ">Check this</a>! <br>";
              }
            }
            row[col.visits] = visits[matchedAt];
          }
          else if (mode === "vuokrattavat") {
            let oldPrice = row[col['price']];
            if (oldPrice != curPrice) {
              let priceDiff = (oldPrice - curPrice);
              row[col['dropped by']] += priceDiff;
              row[col['price']] = curPrice;
            }
          }
        }
        else {  //b. oldID not found on curIDs (flat posting gone)
          row[col.poistunut] = "x";
          row[col['days till sold']] = daysBetweenTwoDates(pubDate);
        }
      }
    });
    sh.getRange("A1").getDataRegion().setValues(data);
    if (msg) { //Add telegram if you want (take as a template the tokmanni code)
      GmailApp.sendEmail("juan.alonso@trimble.com", "Oikotie price dropped!", "", { htmlBody: msg });
    }

    //c. Check for newID matches in oldIDs -> add new row to sheet
    curIDs.forEach((curID, idx) => {
      if (oldIDs.indexOf(curID) == -1) { //a. curID not found on oldIDs (new ID -> insert extra row in the sheet)  
        c = cards[idx];
        extr = extractHtmlElements(c.id, mode);
        if (mode == "myyttävät") {
          out.push([c.id, c.buildingData.year, c.buildingData.address, c.size, c.rooms]
            .concat(extr.slice(0, 11), c.url, c.published, c.visits, new Array(gap), extr.slice(11), c.description, c.roomConfiguration));
        }
        else if (mode == "vuokrattavat") {
          out.push([c.id, c.buildingData.year, c.buildingData.address, c.size, c.rooms, c.price, c.published, c.url, c.visits, c.description, c.roomConfiguration].concat(extr))
        }
      }
    });
    if (out.length !== 0) {//Write new flats at the bottom of sheet
      var lastRow = sh.getLastRow();
      sh.getRange(lastRow + 1, 1, out.length, out[0].length).setValues(out); //TypeError: Cannot read property 'length' of undefined
    }
    if (sh.getName() !== "mL") {
      createStaticMapAndInsert(coordinates, sh); //Create a map with markers & insert it
    }
  }
}



function createStaticMapAndInsert(coordinates, sh) {
  //Delete existing ones
  // let images = 
  sh.getImages().forEach(i => i.remove());
  // for (var i = 0; i < images.length; i++) {
  // images[i].remove();
  // }
  //Create map
  let map = Maps.newStaticMap().setMapType(Maps.StaticMap.Type.SATELLITE);
  map.setMarkerStyle(Maps.StaticMap.MarkerSize.TINY, Maps.StaticMap.Color.RED, 'T');   //TINY, MID (largest, only one with label/3rd param. visible), SMALL 
  coordinates.forEach(row => map.addMarker(row[0], row[1]))
  let blob = map.getBlob();
  //Insert in sheet
  sh.insertImage(blob, 1, sh.getLastRow() + 1);
}

function copyHeader(ss,sh,mode){
  let src = (mode === "vuokrattavat")?ss.getSheetByName("vI"):ss.getSheetByName("mI");
  src.getRange("1:1").copyTo(sh.getRange("A1"), SpreadsheetApp.CopyPasteType.PASTE_NORMAL);
}

function populateSheetFirstTime(sh, cards, mode) { //When setting up a sheet for the first time
  var out = [], extr = [];
  for (c of cards) {
    extr = extractHtmlElements(c.id, mode, c.url);
    if (mode == "myyttävät") {
      out.push([c.id, c.buildingData.year, c.buildingData.address, c.size, c.rooms]
        .concat(extr.slice(0, 11), c.url, c.published, c.visits, new Array(gap), extr.slice(11), c.description, c.roomConfiguration));
    }
    else if (mode == "vuokrattavat") {
      out.push([c.id, c.buildingData.year, c.buildingData.address, c.size, c.rooms, c.price, c.published, c.url, c.visits, c.description, c.roomConfiguration].concat(extr))
    }
  }
  sh.getRange(sh.getLastRow() + 1, 1, out.length, out[0].length).setValues(out);
}

function getAllCards(url) { //Only one write to sheet: 46s 
  var j = getJSON(url);
  var allCards = j.cards;

  for (let i = 24; i <= j.found; i += 24) {
    j = getJSON(url, i);
    allCards = allCards.concat(j.cards);
  }
  return allCards;
}

function getJSON(url, offset = 0) { //Get link not from map view but list
  var cuidTokenTime = getCuidTokenTime();
  var u = "https://asunnot.oikotie.fi/api/cards?cardType=" + url;
  if (offset) {
    u = u.replace(/(?<=offset=)\d+/, offset.toString());
  }
  u += "&sortBy=published_sort_desc";

  var str = UrlFetchApp.fetch(u, {
    "headers": {
      "accept": "application/json",
      // "accept-language": "en-US,en;q=0.9",
      "ota-cuid": cuidTokenTime[0],
      "ota-loaded": cuidTokenTime[2].toString(),
      "ota-token": cuidTokenTime[1],
      // "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin"
    },
    "referrer": "https://asunnot.oikotie.fi/vuokrattavat-asunnot", //myytavat
    "referrerPolicy": "no-referrer-when-downgrade",
    "body": null,
    "method": "GET",
    "mode": "cors",
    "credentials": "omit"//"include"
  });
  var j = JSON.parse(str.getContentText());
  return j;
}

function getCuidTokenTime() {
  var url = "https://asunnot.oikotie.fi/user/get?format=json";
  var str = UrlFetchApp.fetch(url).getContentText();
  var user = JSON.parse(str).user;
  var out = [user.cuid, user.token, user.time];
  return out;
}

function extractHtmlElements(id, mode, url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/" + id, arr = []) { // pass undefined if you need to pass 4th parameter but not 3rd
  var html = UrlFetchApp.fetch(url).getContentText();
  var defaultF = mode.slice(0, 1) == "m" ? ['Kerros', 'Velaton hinta', 'Myyntihinta', 'Neliöhinta', 'Yhtiövastike', 'Rahoitusvastike', 'Hoitovastike', 'Vesimaksu', 'Tulevat remontit', 'Tehdyt remontit', 'Energialuokka', 'Kunto', 'Hissi', 'Tontin omistus', 'Lunastuspykälä'] : ['Kerros', 'Kunto', 'Hissi', 'Energialuokka', 'Vakuus', 'Lisätietoa vakuudesta', 'Vuokra-aika', 'Muut ehdot', 'Tehdyt remontit'];
  var fields = (arr.length > 0) ? arr : defaultF;
  var results = [];

  fields.forEach((field) => {
    var reStr = '(?<=' + field + '<\/dt>\n\t{6}<dd class="info-table__value">).*?(?=<)';
    var regex = new RegExp(reStr, 'g');
    var res = html.match(regex);
    if (res) {
      if (/hinta|vastike|maksu/.test(field)) { //ALT: /^(?:Neliöhinta|Yhtiövastike|Vesimaksu|Hoitovastike|Rahoitusvastike|Myyntihinta|Velaton hinta)$/
        res = toNumber(res[0]);
      }
    }
    results.push(res);
  });
  return results;
}

function toNumber(input) { //First removes everything that isn't a digit (except for comma), then replaces comma for period
  return Number(input.replace(/[^,\d]/g, '').replace(/\,/g, '.'));
}

function daysBetweenTwoDates(pub) {  //Using moment.js
  eval(UrlFetchApp.fetch('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js').getContentText());
  var a = moment(new Date(), 'D/M/YYYY');
  var b = moment(new Date(pub), 'D/M/YYYY');
  return a.diff(b, 'days'); //59
}
