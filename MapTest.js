function myFunction() {
  let map = Maps.newStaticMap().beginPath()
    .addMarker(60.164068546223, 24.858769250043)
    .addMarker(60.150395579491, 24.880223791159)
    .addMarker(60.15943, 24.88957)
    .endPath().getBlob();//
  let sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("img");

  sh.insertImage(map, 1, 1);
}


function emptySheet() {
  let sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("empty");
  if (sh.getLastRow() === 0) {
    Logger.log("The sheet is currently empty")
  }
}

// function getAllImagesInSheet() {
//   let sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("img");
//   let i = sh.getImages();
//   Logger.log(i) //[com.google.apps.maestro.server.beans.trix.impl.OverGridImageApiAdapter@17fce0b7]
// }

function replacingOffsetInUrl(){
  let u = 'https://asunnot.oikotie.fi/api/cards?cardType=101&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D%5D&offset=022&roomCount%5B%5D=3&roomCount%5B%5D=4&size%5Bmin%5D=50&sortBy=published_sort_desc'
  u=u.replace(/(?<=offset=)\d+/,"256");
  Logger.log( u)
}
