//INSTRUCTIONS FOR NEW ADDITIONS
//1.- Select filters on oikotie (not map, but list)
//2.- copy fetch address (select XHR filter)
//3.- remove &offset=0 (added manually later)

//Revise XMLParser here to see if it can be improved/simplified: https://script.google.com/home/projects/1kv5piUB8NRJrBaYhAanj75qhyPUdbAuBUuBoimfp_Xfy1b51QnNps6Gm/edit
//Alts:
//if (field == "Velaton hinta" || field == "Myyntihinta" || field == "Neliöhinta" || field == "Yhtiövastike" || field == "Vesimaksu" || field == "Hoitovastike" || field == "Rahoitusvastike") { //OR var arr = ['Neliöhinta', 'Yhtiövastike', 'Rahoitusvastike', 'Hoitovastike', 'Vesimaksu']; if(arr.includes(field));

// function testIncludes() {
//   var str = "Rahoitusvastike"
//   const permittedWords = ["Neliöhinta", "Yhtiövastike", "Vesimaksu", "Hoitovastike", "Rahoitusvastike"];
//   if (permittedWords.includes(str)) {
//     console.log("made it")
//   }
// }

// function testExtractHtml() {
//   Logger.log(extractHtmlElements("15613979"));
// }

//Turn array into object 
// obj = Object.assign({},row); 
// obj2 = {...row};
//Result: {8=url, 5=year, 1=address, 3=size, 7=description, 13=watching, 6=roomConfig, 11=price history, 0=id, 12=poistunut, 9=published, 4=rooms, 2=price, 10=visits}

//Useful formulas:
//slice(0,-6) equivalent: (for deleting the " € / m" & " € / kk" -the extra zero at the end gets ignored by google sheets)
//=IFERROR(ArrayFormula(left(J2:M164;len(J2:M164)-6));"")



// function getKerrosForOldEntries(){
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("AllMyFilter"); //'All' by default, ColExp associate with j=getJSONForWhole...
//   var data = sh.getRange("A2:F19").getValues();
//   data.forEach((row,idx)=>{
//     data[idx][5]=extractHtmlElements(row[0]).slice(0,1); //ALT: row[5] = extract...
//   });
//   sh.getRange(2, 1, data.length, data[0].length).setValues(data);
// }

//(Inefficient) Write each JSON 52.368 s (+6s)
// function get AllFlatsAndPopulate() { //When setting up a sheet for the first time
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("noF"); //Whole Laru, no filters
//   var j = getJSONForWholeLaru(0);
//   populateSheet(j,sh);

//   for (let i = 24; i <= j.found; i += 24) {
//     j = getJSONForWholeLaru(i);
//     populateSheet(j,sh);
//   }
// }

//TESTING FUNCTIONS
// function callerExtract() { //For testing extractHtmlElements function
//   var ids = [15511354, 15574965, 15584483, 15593397, 15593516, 15597485, 15602509, 15607974, 15609459, 15610880, 15611485, 15612236, 15613979, 15614679, 15612992, 15615197, 15616368, 15620751];
//   var out = [], extr = [];
//   ids.forEach(id => {
//     extr = extractHtmlElements(id);
//     out.push(extr);
//   });
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("Extra"); //for testing purposes
//   sh.getRange(sh.getLastRow() + 1, 1, out.length, out[0].length).setValues(out);
// }

//WORKING ALTERNATIVE OF SHEET THAT POPULATES INFO FROM id (and sends mail if price changes)

// function onEditSpecial(e) { //When adding id's to Watch sheet
//   var rng = e.range;
//   var col = rng.getColumn();
//   var id = e.value;

//   if (col === 1 && id) {
//     var out = getAddressPriceDate(id);
//     var rng2 = rng.offset(0, 1, 1, 3).getA1Notation(); //A5 -> B5:D5
//     var sh = e.source.getSheets()[0];
//     sh.getRange(rng2).setValues([out]);
//   }
// }

// function getAddressPriceDate(id) { //Populate flat info
//   var url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/" + id;
//   var str = UrlFetchApp.fetch(url).getContentText();
//   if (str) {
//     let r = /(?<=ce" content=")\d{6}(?=">)|(?<=hed" content=").*(?=">)|(?<="streetAddress":")[a-zA-Zäö]*\s\d+/g;
//     let arr = str.match(r); //Select street address: (?<="streetAddress":")[a-zA-Z]*\s\d+
//     return arr; //Logger.log(arr); //[Lounaisväylä 8, 444000, 2020-03-28 11:06:22]
//   }
// }

// function checkForPriceChanges() { //Detect all ids in col A, search once per day and extract price, if changed -> write new price + days since published + email alert
//   let sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("Watch");
//   let colAnB = sh.getRange("A2:B2").getDataRegion(SpreadsheetApp.Dimension.ROWS).getValues(); //id = [] (the whole 1st col)
//   let priceCol = 3, newPriceCol = 5, msg = "";
//   let id = colAnB.slice(1).map(x => x[0]); //[15597485, 15593397] //remove header, turn vertical array to horizontal

//   id.forEach((i, idx) => {
//     let row = idx + 2;
//     let oldPrice = sh.getRange(row, priceCol).getValue(); //440000 number
//     var url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/" + i;
//     var str = UrlFetchApp.fetch(url).getContentText();

//     if (str) {
//       let r2 = /(?<=SAC:price" content=")\d{6}|(?<=hed" content=").*(?=">)/gm;
//       let res = str.match(r2); //[169000, 2020-02-07 11:51:16]
//       let newPrice = res[0]; // "440000" string
//       if (res && newPrice != oldPrice) {
//         let address = colAnB.slice(1).map(x => x[1]); //[Lounaisväylä 8, Melojantie 2]
//         let pub = res[1]; //7-2-2020
//         let diff = daysBetweenTwoDates(pub);
//         let out = [res[0], diff];
//         sh.getRange(row, newPriceCol, 1, 2).setValues([out]); //TBC
//         let priceDiff = oldPrice - newPrice;
//         msg += address[idx] + " has lowered its price by " + priceDiff + "€! <a href=" + url + ">Check this</a>! <br>";
//       }
//     }
//   }
//   );
//   if (msg) {
//     GmailApp.sendEmail("juan.alonso@trimble.com", "Oikotie price dropped!", "", { htmlBody: msg });
//   }
// }

//AUXILIARY FUNCTIONS/TESTS

// function transpose(a) { return Object.keys(a[0]).map(function (c) { return a.map(function (r) { return r[c]; }); }); }

// function writeVertArr() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("test");
//   //Create vertical array
//   var arr = Array(10).fill([""]); //[[], [], [], (...) []] //Be careful, all cells reference the same [""], so arr[5][0]=88 will result in: [[88],[88],...]
//   arr[5] = [88]; //[[], [], [], [], [], [55.0], [], [], [], []]
//   sh.getRange(1, 1, 10, 1).setValues(arr);

//   //Initiate an empty multidimensional array (5x8)
//   var mdarr = Array(5).fill("").map(x => Array(8).fill(""));//with 0s: var mdarr=Array(5).fill(0).map(x=>Array(8).fill(0));
//   mdarr[4][1] = 88;
//   mdarr[2][7] = 77;
//   sh.getRange(1, 1, mdarr.length, mdarr[0].length).setValues(mdarr);
//   // Logger.log(mdarr); //[[, , , , , , , ], [, , , , , , , ], [, , , , , , , 77.0], [, , , , , , , ], [, 88.0, , , , , , ]]
//   //0s: [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 77.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 88.0, (...)]]
// }

// function testGetDataRegion() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("All");
//   // var dataRange = sh.getRange("A1").getDataRegion(); //.getA1Notation() A1:M13
//   // Logger.log(dataRange.getValues());
//   var headers = sh.getRange("A1").getDataRegion(SpreadsheetApp.Dimension.COLUMNS).getValues();
//   Logger.log(headers) //[[id, address, price, size, rooms, year, roomConfig, description, url, published, visits, new price, poistunut]]
// }

// function testEmptyDataRegion(){
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("sss"); 
//   var data = sh.getRange("A1").getDataRegion().getValues();
//   if(data.length==1){
//     Logger.log("Hola")
//   }
//   // Logger.log(data); //[[]]
// }

// function testEmptyVerticalArray(){
//   var vertArr = Array(10).fill("").map(x => Array(2).fill(""));
//   Logger.log(vertArr.length + " "+vertArr[0].length);
//   vertArr[2][1]="hola";
//   Logger.log(vertArr.length + " "+vertArr[0].length);
// }

// function testTernaryOperator() {
//   var str = "569 000 €";
//   var str2 = "333 000 €";
//   (str) ? str += "/" + str2 : str += str2;
//   Logger.log(str);

//   var str = "";
//   var str2 = "333 000 €";
//   (str) ? str += "/" + str2 : str += str2;
//   Logger.log(str);
// }

// function testForEach(){
  // var arr = [[1,2,3,4,5,6],[7,8,9,10]];
  // arr.forEach((row,idx)=>{
  //   row[0]=33;
  // });
  // Logger.log(arr); //[[33.0, 2.0, 3.0, 4.0, 5.0, 6.0], [33.0, 8.0, 9.0, 10.0]]

  // var arr = [6,7,8,9,10,11];
  // arr = arr.map((x,idx)=>idx);
  // arr.forEach((x,idx)=>arr[idx]=idx);
  // Logger.log(arr); //[0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
// }

// function daysBetweenTwoDates(pub) {  
//   //Option 1: moment.js
//   eval(UrlFetchApp.fetch('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js').getContentText());
//   var a = moment(new Date(), 'D/M/YYYY');
//   var b = moment(new Date(pub), 'D/M/YYYY');
//   var diffDays = a.diff(b, 'days'); //59 days
//   return diffDays;
//   //Option 2: diference of ms
//   // var res = [169000, "2020-02-07 11:51:16"];
//   // const ms_per_day = 1000 * 60 * 60 * 24;
//   // let pub = new Date(res[1]); //7-2-2020
//   // let today = new Date();     //6-4-2020
//   // let diff = (today.getTime()-pub.getTime())/ms_per_day; //59 days   // Logger.log(Math.round(diff));
// }


//ALTS for step c)
  //a2. Using headers (long)
  // var arr = [];
  // headers.forEach((x)=>{
  //   if(x=="address"){arr.push(c.buildingData.address);}
  //   else if(x=="year"){arr.push(c.buildingData.year);}
  //   else{arr.push(c[x]);}
  // });
  //b. Explicit
  // var objLen = Object.keys(col).length;
  // var arr = new Array(objLen); arr[col.id]=c.id; arr[col.address]=c.buildingData.address; arr[col.price]=c.price;arr[col.rooms]=c.rooms;arr[col.year]=c.buildingData.year;arr[col.roomConfiguration]=c.roomConfiguration; arr[col.description]=c.description;arr[col.url]=c.url;arr[col.published]=c.published;arr[col.visits]=c.visits;
  // out.push(arr);
  //No support for moving columns around:
  // out.push([c.id, c.buildingData.address, c.price, c.size, c.rooms, c.buildingData.year, c.roomConfiguration, c.description, c.url, c.published, c.visits]);

//OLD VERSION 2 (before adding indiv posting scrape) 27.4.
// @ts-nocheck
// function testExtractHtmlElements(){
//   var url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/15597485";
//   var html = UrlFetchApp.fetch(url).getContentText();
//   //var re = new RegExp('(?<=<dd class="info-table__value">)[^<].*?(?=<)', 'g');
//   var re = /(?<=<dd class="info-table__value">)\d*\s\€.*(?=<\/)|(?<=Energialuokka<\/dt>\n\t{6}<dd class="info-table__value">|Tulevat remontit<\/dt>\n\t{6}<dd class="info-table__value">|Tehdyt remontit<\/dt>\n\t{6}<dd class="info-table__value">|Myyntihinta<\/dt>\n\t{6}<dd class="info-table__value">|Neliöhinta<\/dt>\n\t{6}<dd class="info-table__value">|Lunastuspykälä<\/dt>\n\t{6}<dd class="info-table__value">).*?(?=<)/g;
//   var extr = html.match(re); //[Tulevat remontit, Tehdyt remontit, Myyntihinta, Neliöhinta, Lunastuspykälä, Rahoitusvastike, Hoitovastike, Yhtiövastike, Vesimaksu, Energialuokka]
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("indiv");
//   sh.getRange(sh.getLastRow()+1,1,1,extr.length).setValues([extr]);
// }

// function getFlats() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("All");
//   var data = sh.getRange("A1").getDataRegion().getValues();
//   var j = getJSON(); //UrlFetch
//   var out = [], col = {}, headers = [];

//   if (data.length == 1) { //Run on an empty sheet (initial setup)
//     for (c of j.cs) {
//       out.push([c.id, c.buildingData.address, c.price, c.size, c.rooms, c.buildingData.year, c.roomConfiguration, c.description, c.url, c.published, c.visits]);
//     }
//     sh.getRange(2, 1, out.length, out[0].length).setValues(out);
//   }
//   else { //There's info on the sheet already
//     var curIDs = [], curPrices = [], visits = [], poistunut = "", watching = "", msg = "", oldID = 0, oldIDs = [];
//     for (c of j.cards) {
//       curIDs.push(c.id);
//       curPrices.push(c.price);
//       visits.push(c.visits);
//     }
//     data.forEach((row, idx) => {
//       if (!idx) {
//         row.forEach((x, idx) => {
//           col[x] = idx; //{price=2.0, published=9.0,description=7.0, address=1.0, url=8.0, poistunut=12.0, id=0.0, size=3.0, visits=10.0, year=5.0, rooms=4.0,roomConfiguration=6.0...}
//           headers.push(x); //[id,	address,	price,	size,	rooms,	year,	roomConfiguration,	description,	url,	published,	visits,	price history,	poistunut,	watching]
//         });
//       }
//       poistunut = row[col.poistunut];
//       oldID = row[col.id];
//       oldIDs.push(oldID);

//       if (poistunut == "") { //Check for oldID matches in curIDs
//         var match = curIDs.indexOf(oldID);
//         var pubDate = row[col.published];

//         if (match !== -1) { //a. match found -> check price
//           var curPrice = curPrices[match];
//           var oldPrice = row[col.price];
//           if (oldPrice != curPrice) {
//             var diff = daysBetweenTwoDates(pubDate);
//             row[col['price history']] += oldPrice + " for " + diff + " days/"; //priceHistory
//             row[col.price] = curPrice; //price
//             //Send alert if watching
//             watching = row[col.watching];
//             if (watching == "x") {
//               priceDiff = oldPrice - newPrice;
//               msg += row[col.price] + " has lowered its price by " + priceDiff + "€! <a href=" + row[col.published] + ">Check this</a>! <br>";
//             }
//           }
//           row[col.visits] = visits[match];
//         }
//         else {  //b. oldID not found on curIDs (flat posting gone)
//           row[col.poistunut] = "x";
//           row[col['days till sold']] = daysBetweenTwoDates(pubDate);
//         }
//       }
//     });
//     sh.getRange("A1").getDataRegion().setValues(data);
//     if (msg) { //Add telegram if you want (take as a template the tokmanni code)
//       GmailApp.sendEmail("juan.alonso@trimble.com", "Oikotie price dropped!", "", { htmlBody: msg });
//     }

//     //c. Check for newID matches in oldIDs -> add new row to sheet
//     curIDs.forEach((curID, idx) => {
//       if (oldIDs.indexOf(curID) == -1) { //a. curID not found on oldIDs (new ID -> insert extra row in the sheet)  
//         c = j.cards[idx];
//         var arr = [];
//         headers.forEach((x) => { //Allowing for column position change, addressing 2 cases where header name != card property
//           (x == "address") ? arr.push(c.buildingData.address) : (x == "year") ? arr.push(c.buildingData.year) : arr.push(c[x]);
//         });
//         out.push(arr);
//       }
//     });
//     if (out.length !== 0) {//Write new flats at the bottom of sheet
//       var lastRow = sh.getLastRow();
//       sh.getRange(lastRow + 1, 1, out.length, out[0].length).setValues(out); //TypeError: Cannot read property 'length' of undefined
//     }
//   }
// }

// function getJSON() {
//   var cuidTokenTime = getCuidTokenTime();
//   var str = UrlFetchApp.fetch("https://asunnot.oikotie.fi/api/cards?cardType=100&conditionType%5B%5D=1&conditionType%5B%5D=2&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D,%5B14714,5,%2200340,+Helsinki%22%5D%5D&lotOwnershipType%5B%5D=1&offset=0&price%5Bmax%5D=600000&price%5Bmin%5D=150000&roomCount%5B%5D=3&size%5Bmin%5D=35&sortBy=published_sort_desc", { "credentials": "omit", "headers": { "accept": "application/json", "ota-cuid": cuidTokenTime[0], "ota-loaded": cuidTokenTime[2].toString(), "ota-token": cuidTokenTime[1], "sec-fetch-dest": "empty" }, "referrer": "https://asunnot.oikotie.fi/myytavat-asunnot?conditionType%5B%5D=1&conditionType%5B%5D=2&locations=%5B%5B1669,4,%22Lauttasaari,%20Helsinki%22%5D,%5B14714,5,%2200340,%20Helsinki%22%5D%5D&lotOwnershipType%5B%5D=1&price%5Bmax%5D=600000&price%5Bmin%5D=150000&size%5Bmin%5D=35&roomCount%5B%5D=3&cardType=100", "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" });
//   var j = JSON.parse(str.getContentText());
//   return j;
// }

// function getCuidTokenTime() {
//   var url = "https://asunnot.oikotie.fi/user/get?format=json";
//   var str = UrlFetchApp.fetch(url).getContentText();
//   var user = JSON.parse(str).user;
//   var out = [user.cuid, user.token, user.time];
//   return out;
//   //Can't understand why this won't work: var out = str.match(/(?<="cuid": ")\w+(?=")/g); //complete formula: (?<="cuid": "|"token": "|"time": )\w+(?="|,)  // Logger.log(out);
// }

// //Revise XMLParser here to see if it can be improved/simplified: https://script.google.com/home/projects/1kv5piUB8NRJrBaYhAanj75qhyPUdbAuBUuBoimfp_Xfy1b51QnNps6Gm/edit

// function daysBetweenTwoDates(pub) {  //Using moment.js
//   eval(UrlFetchApp.fetch('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js').getContentText());
//   var a = moment(new Date(), 'D/M/YYYY');
//   var b = moment(new Date(pub), 'D/M/YYYY');
//   return a.diff(b, 'days'); //59
// }

// //Turn array into object 
// // obj = Object.assign({},row); 
// // obj2 = {...row};
// //Result: {8=url, 5=year, 1=address, 3=size, 7=description, 13=watching, 6=roomConfig, 11=price history, 0=id, 12=poistunut, 9=published, 4=rooms, 2=price, 10=visits}


//OLD VERSION
// function getOldIDsPrices() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("All");
//   var data = sh.getRange("A1").getDataRegion().getValues();
//   var oldIDs=[], oldPrices=[];
//   for(r of data){
//     if(r[12]==""){          //poistunut col: 12
//       oldIDs.push(r[0]);    // id column: 0
//       oldPrices.push(r[2]); //price column: 2
//     }
//   }
//   // var oldIDs = sh.getRange("A1").getDataRegion(SpreadsheetApp.Dimension.ROWS).getValues().slice(1).flat(); //length 12
//   // var oldPrices = sh.getRange("C1").getDataRegion(SpreadsheetApp.Dimension.ROWS).getValues().slice(1).flat(); //length 12
//   var lastRow = sh.getLastRow();
//   return [oldIDs, oldPrices, lastRow];
// }

// function getFlats() { //Fetch
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("All");
//   var [oldIDs, oldPrices, lastRow] = getOldIDsPrices();
//   var j = getJSON();
//   var out = [], col = 1;

//   if (oldIDs.length == 0) { //Initial setup
//     //Extract values
//     for (c of j.cards) {
//       out.push([c.id, c.buildingData.address, c.price, c.size, c.rooms, c.buildingData.year, c.roomConfiguration, c.description, c.url, c.published, c.visits]);
//       sh.getRange(2, 1, out.length, out[0].length).setValues(out);
//     }
//   }
//   else {    //There's info on the sheet already
//     var headers = sh.getRange("A1").getDataRegion(SpreadsheetApp.Dimension.COLUMNS).getValues(); //.getDataRegion() //for the whole table equivalent to ^a command
//     var newPricesPoistunut = Array(lastRow-1).fill("").map(x => Array(2).fill(""));    // var newPrices = Array(lastRow).fill([""]);

//     for (c of j.cards) { //Check for matches in oldIDs for curID  
//       var curID = c.id;
//       var matchIdx = oldIDs.indexOf(curID);
//       var changed = false;

//       if (matchIdx == -1) { //a. curID not found on oldIDs (new ID -> insert extra row in the sheet)  
//         out.push([c.id, c.buildingData.address, c.price, c.size, c.rooms, c.buildingData.year, c.roomConfiguration, c.description, c.url, c.published, c.visits])
//       }
//       else {             //b. existing ID
//         var curPrice = c.price;
//         if (oldPrices[matchIdx] != curPrice) {
//           newPricesPoistunut[matchIdx][0] = curPrice;
//           changed=true;
//         }
//       }     
//     }

//     //c. Check for matches in curIDs for oldID 
//     var curIDs = j.cards.map(c => c.id);
//     oldIDs.forEach((id, idx) => {
//       var matchIdx = curIDs.indexOf(id);
//       if (matchIdx == -1) {  //oldID not found on curIDs (flat posting gone)
//         newPricesPoistunut[idx][1] = "x";
//         changed=true;
//       }
//     }); 

//     if(changed){//Insert newPricesPoistunut
//       var col = headers[0].indexOf("new price");
//       sh.getRange(2,col+1,newPricesPoistunut.length,2).setValues(newPricesPoistunut); //alt than 2: newPricesPoistunut[0].length
//     }
//     if(out.length !== 0){//Write new flats at the bottom of sheet
//       sh.getRange(lastRow+1,1,out.length,out[0].length).setValues(out); //TypeError: Cannot read property 'length' of undefined
//     }
//   }
// }
// //NEXT: Explore making this more efficient by reducing calls to SpreadsheetApp from 2 to 1 (big 2D array loop I assume)

// function getJSON() {
//   var cuidTokenTime = getCuidTokenTime();
//   var str = UrlFetchApp.fetch("https://asunnot.oikotie.fi/api/cards?cardType=100&conditionType%5B%5D=1&conditionType%5B%5D=2&limit=24&locations=%5B%5B1669,4,%22Lauttasaari,+Helsinki%22%5D,%5B14714,5,%2200340,+Helsinki%22%5D%5D&lotOwnershipType%5B%5D=1&offset=0&price%5Bmax%5D=600000&price%5Bmin%5D=150000&roomCount%5B%5D=3&size%5Bmin%5D=35&sortBy=published_sort_desc", { "credentials": "omit", "headers": { "accept": "application/json", "ota-cuid": cuidTokenTime[0], "ota-loaded": cuidTokenTime[2].toString(), "ota-token": cuidTokenTime[1], "sec-fetch-dest": "empty" }, "referrer": "https://asunnot.oikotie.fi/myytavat-asunnot?conditionType%5B%5D=1&conditionType%5B%5D=2&locations=%5B%5B1669,4,%22Lauttasaari,%20Helsinki%22%5D,%5B14714,5,%2200340,%20Helsinki%22%5D%5D&lotOwnershipType%5B%5D=1&price%5Bmax%5D=600000&price%5Bmin%5D=150000&size%5Bmin%5D=35&roomCount%5B%5D=3&cardType=100", "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" });
//   var j = JSON.parse(str.getContentText());
//   return j;
// }

// function getCuidTokenTime() {
//   var url = "https://asunnot.oikotie.fi/user/get?format=json";
//   var str = UrlFetchApp.fetch(url).getContentText();
//   var user = JSON.parse(str).user;
//   var out = [user.cuid, user.token, user.time];
//   return out;
//   //Can't understand why this won't work: var out = str.match(/(?<="cuid": ")\w+(?=")/g); //complete formula: (?<="cuid": "|"token": "|"time": )\w+(?="|,)  // Logger.log(out);
// }

// function onEditSpecial(e) {
//   var rng = e.range;
//   var col = rng.getColumn();
//   var id = e.value;

//   if (col === 1 && id) {
//     var out = getAddressPriceDate(id);
//     var rng2 = rng.offset(0, 1, 1, 3).getA1Notation(); //A5 -> B5:D5
//     var sh = e.source.getSheets()[0];
//     sh.getRange(rng2).setValues([out]);
//   }
// }

// function getAddressPriceDate(id) {
//   var url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/" + id;
//   var str = UrlFetchApp.fetch(url).getContentText();
//   if (str) {
//     let r = /(?<=ce" content=")\d{6}(?=">)|(?<=hed" content=").*(?=">)|(?<="streetAddress":")[a-zA-Zäö]*\s\d+/g;
//     let arr = str.match(r); //Select street address: (?<="streetAddress":")[a-zA-Z]*\s\d+
//     return arr; //Logger.log(arr); //[Lounaisväylä 8, 444000, 2020-03-28 11:06:22]
//   }
// }

// function checkForPriceChanges() { //Detect all ids in col A, search once per day and extract price, if changed -> write new price + days since published + email alert
//   let sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("Watch");
//   let colAnB = sh.getRange("A2:B2").getDataRegion(SpreadsheetApp.Dimension.ROWS).getValues(); //id = [] (the whole 1st col)
//   let priceCol = 3, newPriceCol = 5, msg = "";
//   let id = colAnB.slice(1).map(x => x[0]); //[15597485, 15593397] //remove header, turn vertical array to horizontal

//   id.forEach((i, idx) => {
//     let row = idx + 2;
//     let oldPrice = sh.getRange(row, priceCol).getValue(); //440000 number
//     var url = "https://asunnot.oikotie.fi/myytavat-asunnot/helsinki/" + i;
//     var str = UrlFetchApp.fetch(url).getContentText();

//     if (str) {
//       let r2 = /(?<=SAC:price" content=")\d{6}|(?<=hed" content=").*(?=">)/gm;
//       let res = str.match(r2); //[169000, 2020-02-07 11:51:16]
//       let newPrice = res[0]; // "440000" string
//       if (res && newPrice != oldPrice) {
//         let address = colAnB.slice(1).map(x => x[1]); //[Lounaisväylä 8, Melojantie 2]
//         const ms_per_day = 1000 * 60 * 60 * 24;
//         let pub = new Date(res[1]); //7-2-2020
//         let today = new Date();     //6-4-2020
//         let diff = (today.getTime() - pub.getTime()) / ms_per_day; //59 days
//         let out = [res[0], Math.round(diff) + " days"];
//         sh.getRange(row, newPriceCol, 1, 2).setValues([out]); //TBC
//         let priceDiff = oldPrice - newPrice;
//         msg += address[idx] + " has lowered its price by " + priceDiff + "€! <a href=" + url + ">Check this</a>! <br>";
//       }
//     }
//   }
//   );
//   if (msg) {
//     GmailApp.sendEmail("juan.alonso@trimble.com", "Oikotie price dropped!", "", { htmlBody: msg });
//   }
// }

// //Revise XMLParser here to see if it can be improved/simplified 
// // https://script.google.com/home/projects/1kv5piUB8NRJrBaYhAanj75qhyPUdbAuBUuBoimfp_Xfy1b51QnNps6Gm/edit


// function daysBetweenTwoDates() {
//   var res = [169000, "2020-02-07 11:51:16"];

//   //Option 1: moment.js
//   eval(UrlFetchApp.fetch('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js').getContentText());
//   var a = moment(new Date(), 'D/M/YYYY');
//   var b = moment(new Date(res[1]), 'D/M/YYYY');
//   var diffDays = a.diff(b, 'days'); //59 days

//   //Option 2: diference of ms
//   // const ms_per_day = 1000 * 60 * 60 * 24;
//   // let pub = new Date(res[1]); //7-2-2020
//   // let today = new Date();     //6-4-2020
//   // let diff = (today.getTime()-pub.getTime())/ms_per_day; //59 days   // Logger.log(Math.round(diff));
// }

// function transpose(a) { return Object.keys(a[0]).map(function (c) { return a.map(function (r) { return r[c]; }); }); }

// function writeVertArr() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("test");
//   //Create vertical array
//   var arr = Array(10).fill([""]); //[[], [], [], (...) []] //Be careful, all cells reference the same [""], so arr[5][0]=88 will result in: [[88],[88],...]
//   arr[5] = [88]; //[[], [], [], [], [], [55.0], [], [], [], []]
//   sh.getRange(1, 1, 10, 1).setValues(arr);

//   //Initiate an empty multidimensional array (5x8)
//   var mdarr = Array(5).fill("").map(x => Array(8).fill(""));//with 0s: var mdarr=Array(5).fill(0).map(x=>Array(8).fill(0));
//   mdarr[4][1] = 88;
//   mdarr[2][7] = 77;
//   sh.getRange(1, 1, mdarr.length, mdarr[0].length).setValues(mdarr);
//   // Logger.log(mdarr); //[[, , , , , , , ], [, , , , , , , ], [, , , , , , , 77.0], [, , , , , , , ], [, 88.0, , , , , , ]]
//   //0s: [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 77.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 88.0, (...)]]
// }

// function testGetDataRegion() {
//   var sh = SpreadsheetApp.openById("1DcmVmvWhFR6Op2fzhJ3ahJ1pmSThfb-GGbF-GP3nmcQ").getSheetByName("All");
//   // var dataRange = sh.getRange("A1").getDataRegion(); //.getA1Notation() A1:M13
//   // Logger.log(dataRange.getValues());
//   var headers = sh.getRange("A1").getDataRegion(SpreadsheetApp.Dimension.COLUMNS).getValues();
//   Logger.log(headers) //[[id, address, price, size, rooms, year, roomConfig, description, url, published, visits, new price, poistunut]]
// }